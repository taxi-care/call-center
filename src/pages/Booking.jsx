import React from "react";
import InforCustomer from "../components/Booking/InforCustomer";
import MapboxMap from "../components/Map/MapboxMap";
import TripTable from "../components/Booking/TripTable";

function Booking() {
  return (
    <div>
      <div className="grid grid-cols-1 md:grid-cols-3">
        <div className="p-5">
          <h2 className="text-[20px] font-semibold">Booking</h2>
          <InforCustomer />
        </div>
        <div className="col-span-2 p-5">
          <h2 className="text-[20px] font-semibold">Map</h2>
          <MapboxMap />
        </div>
      </div>
      <div>
        <TripTable />
      </div>
    </div>
  );
}

export default Booking;
