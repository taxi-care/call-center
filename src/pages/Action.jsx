import React, { useContext, useEffect, useState } from "react";
import { BaseUrl, getRequest, postRequest } from "../utils/services";
import { AuthContext } from "../context/AuthContext";
import TripEnd from "../components/Action/TripEnd";
import { TripContext } from "../context/TripContext";

function Action() {
  const { user } = useContext(AuthContext);
  const { tripChangeStatus } = useContext(TripContext);
  const [tripsHistory, setTripsHistory] = useState();
  useEffect(() => {
    const getTripsHistory = async () => {
      const res = await postRequest(
        `${BaseUrl}/trips/get-all-trip`,
        JSON.stringify({ userId: user?._id })
      );
      if (res.error) {
        return console.log(res);
      }
      setTripsHistory(res.metadata);
    };
    getTripsHistory();
  }, []);

  useEffect(() => {
    console.log(tripsHistory);
    console.log("---------------------");
    console.log(tripChangeStatus);
    if (tripsHistory && tripChangeStatus) {
      setTripsHistory((prev) =>
        prev.map((item) =>
          item._id === tripChangeStatus._id ? tripChangeStatus : item
        )
      );
    }
  }, [tripChangeStatus]);
  return (
    <div className="mt-4">
      <div>
        <h1 className="text-xl font-semibold ml-3">Action</h1>
        <div className="mt-3 flex flex-col items-center gap-4 pb-[120px]">
          {tripsHistory ? (
            tripsHistory?.map((item, index) => {
              return <TripEnd key={index} trip={item} />;
            })
          ) : (
            <h1 className="text-xl">Chưa có chuyến đi nào</h1>
          )}
        </div>
      </div>
    </div>
  );
}

export default Action;
