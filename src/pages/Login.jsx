import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
function Login() {
  const {
    loginInfor,
    updateLoginInfor,
    loginUser,
    loginError,
    isLoginLoading,
  } = useContext(AuthContext);
  return (
    <div className="flex flex-col items-center h-full relative ">
      <div className="flex justify-center mt-[20%] md:mt-[5%]">
        <img src="/callCenter.png" alt="logo" className="w-[200px] h-auto" />
      </div>
      <h1 className="text-center my-10 text-3xl">Login</h1>
      <div className="flex flex-col gap-5 w-[90%]">
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-full outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Phone number"
          onChange={(e) =>
            updateLoginInfor({ ...loginInfor, phone: e.target.value })
          }
        />
        <input
          type="password"
          className="bg-white p-1 border-[1px] w-full rounded-full outline-none focus:border-[#3422F2] py-3 px-4 text-sm"
          placeholder="Password"
          onChange={(e) =>
            updateLoginInfor({ ...loginInfor, password: e.target.value })
          }
        />
      </div>
      {loginError?.error && (
        <div className="mt-8 w-[90%]">
          <p className="w-full text-red-500 py-3">{loginError?.message}</p>
        </div>
      )}
      <div className="mt-8 w-[90%]">
        <button
          className="w-full bg-[#3422F2] text-white py-3 rounded-full"
          disabled={isLoginLoading}
          onClick={loginUser}
        >
          {isLoginLoading ? "Getting you in..." : "Sign in"}
        </button>
      </div>
    </div>
  );
}
export default Login;
