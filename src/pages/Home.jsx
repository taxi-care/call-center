import React, { useContext, useEffect } from "react";
import NavBar from "../components/NavBar";
import { Outlet } from "react-router-dom";
import { TripContext } from "../context/TripContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/ReactToastify.min.css";

function Home() {
  const { toastMessage } = useContext(TripContext);
  useEffect(() => {
    const addNotification = () => {
      toast[toastMessage.status](toastMessage.message, {
        position: "top-right",
        autoClose: 1000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    };
    if (toastMessage) {
      addNotification();
    }
  }, [toastMessage]);
  return (
    <div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
      <ToastContainer />
      <NavBar />
      <Outlet />
    </div>
  );
}

export default Home;
