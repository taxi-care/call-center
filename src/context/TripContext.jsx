import { createContext, useState, useEffect, useCallback, useRef } from "react";
import { io } from "socket.io-client";
import { useNavigate } from "react-router-dom";
import { postRequest, BaseUrl } from "../utils/services";

export const TripContext = createContext();
export const TripContextProvider = ({ children, user }) => {
  const navigate = useNavigate();

  const [phoneCustomer, setPhoneCustomer] = useState("");
  const [nameCustomer, setNameCustomer] = useState("");
  const [userLocation, setUserLocation] = useState();
  const [source, setSource] = useState("");
  const [sourceCoordinates, setSourceCoordinates] = useState();
  const [destination, setDestination] = useState("");
  const [destinationCoordinates, setDestinationCoordinates] = useState();
  const [selectedCar, setSelectedCar] = useState();
  const [directionData, setDirectionData] = useState([]);
  const [isReadyBooking, setIsReadyBooking] = useState(false);
  const [historyTripByPhone, setHistoryTripByPhone] = useState([]);
  const [socket, setSocket] = useState(null);
  const [toastMessage, setToastMessage] = useState(null);
  const [tripChangeStatus, setTripChangeStatus] = useState();
  const socketRef = useRef(null);
  //Init socket
  useEffect(() => {
    let attempts = 0;

    const connectSocket = () => {
      socketRef.current = io("http://localhost:8000");

      socketRef.current.on("connect", () => {
        console.log("Kết nối socket thành công");
        setSocket(socketRef.current);
        console.log(socketRef.current);
        attempts = 0;
      });

      socketRef.current.on("connect_error", () => {
        console.log("Kết nối socket thất bại, đang thử lại...");
        attempts++;
        if (attempts < 5) {
          setTimeout(connectSocket, 1000); // Thử lại sau 1 giây
        }
      });
    };

    if (user) {
      connectSocket();
    }
    return () => {
      socketRef?.current?.disconnect();
    };
  }, [user]);

  //Add online users
  useEffect(() => {
    if (socket === null) {
      return;
    }
    socket.emit("addNewUser", {
      userId: user?._id,
      role: "customer",
    });
  }, [socket]);

  //listen action from driver
  useEffect(() => {
    if (socket == null) {
      return;
    }
    socket.on("acceptTrip", (res) => {
      setToastMessage({
        status: "success",
        message: `Chuyến đi điều phối đã có tài xế nhận chuyến`,
      });
      setTripChangeStatus(res.trip);
    });
    socket.on("driverCancelTrip", (res) => {
      console.log("tài xế mới hủy chuyến nè");
      setToastMessage({
        status: "warn",
        message: `Tài xế vừa hủy chuyến hệ thống đang tìm tài xế khác`,
      });
      setTripChangeStatus(res);
    });
    socket.on("completedTrip", (res) => {
      setToastMessage({
        status: "success",
        message: `Chuyến đi đã được tài xế hoàn thành`,
      });
      setTripChangeStatus(res.trip);
    });
    return () => {
      socket.off("acceptTrip");
      socket.off("driverCancelTrip");
      socket.off("completedTrip");
    };
  }, [socket]);

  //ReadyBooking
  useEffect(() => {
    if (
      source &&
      sourceCoordinates &&
      destination &&
      destinationCoordinates &&
      selectedCar &&
      directionData &&
      phoneCustomer &&
      nameCustomer
    ) {
      setIsReadyBooking(true);
    } else {
      setIsReadyBooking(false);
    }
  }, [
    source,
    sourceCoordinates,
    destination,
    destinationCoordinates,
    selectedCar,
    directionData,
    phoneCustomer,
    nameCustomer,
  ]);

  //Get Location Call center
  useEffect(() => {
    const getUserLocation = () => {
      navigator.geolocation.getCurrentPosition(function (pos) {
        setUserLocation({
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        });
      });
    };
    getUserLocation();
  }, []);

  //Function Request trip
  const tripRequest = useCallback(async () => {
    const res = await postRequest(
      `${BaseUrl}/trips/create`,
      JSON.stringify({
        source,
        sourceCoordinates: [sourceCoordinates.lng, sourceCoordinates.lat],
        destination,
        destinationCoordinates: [
          destinationCoordinates.lng,
          destinationCoordinates.lat,
        ],
        PhoneCustomer: phoneCustomer,
        NameCustomer: nameCustomer,
        distance: (directionData.routes[0].distance / 1000).toFixed(1),
        price: (
          (directionData.routes[0].distance / 1000) *
          selectedCar.price
        ).toFixed(1),
        userId: user?._id,
        selectedCar: selectedCar.id,
      })
    );
    if (res.error) {
      return console.log(res);
    }

    setSource("");
    setSourceCoordinates();
    setDestination("");
    setDestinationCoordinates();
    setDirectionData();
    setSelectedCar();
    setPhoneCustomer("");
    setNameCustomer("");
    setHistoryTripByPhone([]);
    setToastMessage({
      status: "success",
      message: `Điều phối thành công đợi tài xế nhận chuyến`,
    });
  }, [
    source,
    sourceCoordinates,
    destination,
    destinationCoordinates,
    selectedCar,
    directionData,
  ]);
  return (
    <TripContext.Provider
      value={{
        phoneCustomer,
        setPhoneCustomer,
        nameCustomer,
        setNameCustomer,
        userLocation,
        sourceCoordinates,
        setSourceCoordinates,
        destinationCoordinates,
        setDestinationCoordinates,
        directionData,
        setDirectionData,
        source,
        setSource,
        destination,
        setDestination,
        selectedCar,
        setSelectedCar,
        isReadyBooking,
        tripRequest,
        setHistoryTripByPhone,
        historyTripByPhone,
        toastMessage,
        tripChangeStatus,
      }}
    >
      {children}
    </TripContext.Provider>
  );
};
