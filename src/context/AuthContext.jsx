import { createContext, useCallback, useState, useEffect } from "react";
import { BaseUrl, postRequest, getRequestAuth } from "../utils/services";

export const AuthContext = createContext();

export const AuthContextProvider = ({ children }) => {
  //Init User
  const [user, setUser] = useState();
  //Login
  const [loginError, setLoginError] = useState(null);
  const [isLoginLoading, setIsLoginLoading] = useState(false);
  const [loginInfor, setIsLoginInfor] = useState({
    phone: "",
    password: "",
  });

  //Duy trì đăng nhập với access và refresh token
  useEffect(() => {
    const getUserWithToken = async () => {
      const response = await getRequestAuth(`${BaseUrl}/users/`);
      if (response.error) {
        return console.log(response);
      }
      setUser(response.metadata);
    };
    getUserWithToken();
  }, []);

  //Function Login
  const updateLoginInfor = useCallback((infor) => {
    setIsLoginInfor(infor);
  }, []);

  const loginUser = useCallback(async () => {
    setIsLoginLoading(true);
    setLoginError(null);

    const res = await postRequest(
      `${BaseUrl}/users/login/cc`,
      JSON.stringify(loginInfor)
    );
    setIsLoginLoading(false);

    if (res.error) {
      return setLoginError(res);
    }
    localStorage.setItem(
      "access_token_cc",
      JSON.stringify(res.metadata.access_token)
    );
    localStorage.setItem(
      "refresh_token_cc",
      JSON.stringify(res.metadata.refresh_token)
    );
    const currentUser = await getRequestAuth(`${BaseUrl}/users/`);
    if (currentUser.error) {
      return setLoginError(res);
    }
    setUser(currentUser.metadata);
  }, [loginInfor]);

  const logoutUser = useCallback(() => {
    localStorage.removeItem("access_token_cc");
    localStorage.removeItem("refresh_token_cc");
    setUser(null);
  }, []);

  return (
    <AuthContext.Provider
      value={{
        user,
        loginInfor,
        updateLoginInfor,
        loginUser,
        loginError,
        isLoginLoading,
        logoutUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
