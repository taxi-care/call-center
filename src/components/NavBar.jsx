import React, { useContext, useState } from "react";
import { AuthContext } from "../context/AuthContext";
import { Link } from "react-router-dom";

function NavBar() {
  const [popUpSignOut, setPopUpSignOut] = useState(false);
  const { logoutUser } = useContext(AuthContext);
  return (
    <div className="flex justify-between p-3 px-10 items-center border-b-[1px] shadow-sm">
      <div className="flex gap-10 items-center">
        <img src="/logo.png" alt="logo taxi care" className="w-25 h-20" />
        <div className="hidden md:flex gap-6">
          <Link to="/">
            <h2 className="hover:bg-gray-100 p-2 rounded-md cursor-pointer transition-all">
              Home
            </h2>
          </Link>
          <Link to="/action">
            <h2 className="hover:bg-gray-100 p-2 rounded-md cursor-pointer transition-all">
              Action
            </h2>
          </Link>
          <h2 className="hover:bg-gray-100 p-2 rounded-md cursor-pointer transition-all">
            Help
          </h2>
        </div>
      </div>
      <div
        className="relative cursor-pointer"
        onClick={() => {
          setPopUpSignOut(!popUpSignOut);
        }}
      >
        <img
          src="/profileCC.svg"
          alt="avatar call center"
          className="w-16 h-16"
        />
        {popUpSignOut ? (
          <div className="p-2 shadow-md rounded-md absolute w-[130px] bg-slate-200 left-[-100%] top-[4.5rem]">
            <p
              className="flex gap-3"
              onClick={() => {
                logoutUser();
              }}
            >
              <img src="/signOut.svg" alt="sign out" className="w-5 h-5" />
              <span>Sign Out</span>
            </p>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default NavBar;
