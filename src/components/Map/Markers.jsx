import React, { useContext } from "react";
import Map, { Marker } from "react-map-gl";
import pin from "../../assets/pin.svg";
import { TripContext } from "../../context/TripContext";

function Markers() {
  const { userLocation, sourceCoordinates, destinationCoordinates } =
    useContext(TripContext);
  return (
    <div>
      <Marker
        longitude={userLocation?.lng}
        latitude={userLocation?.lat}
        anchor="bottom"
      >
        <img src={pin} className="w-10 h-10" />
      </Marker>

      {/*Source Makers */}
      {sourceCoordinates ? (
        <Marker
          longitude={sourceCoordinates?.lng}
          latitude={sourceCoordinates?.lat}
          anchor="bottom"
        >
          <div className="p-16 bg-blue-400/50 rounded-full relative top-16">
            <img
              src="/book-location.svg"
              className="w-10 h-10 animate-bounce"
            />
          </div>
        </Marker>
      ) : null}
      {/*Destination Makers */}
      {destinationCoordinates ? (
        <Marker
          longitude={destinationCoordinates?.lng}
          latitude={destinationCoordinates?.lat}
          anchor="bottom"
        >
          <img src="/book-location.svg" className="w-10 h-10" />
        </Marker>
      ) : null}
    </div>
  );
}

export default Markers;
