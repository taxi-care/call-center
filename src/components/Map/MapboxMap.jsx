import { useContext, useRef, useEffect } from "react";
import Map from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { TripContext } from "../../context/TripContext";
import Markers from "./Markers";
import MapBoxRoute from "./MapboxRoute";

const KEY_MAPBOX = import.meta.env.VITE_KEY_MAP;

function MapboxMap() {
  const mapRef = useRef();
  const {
    userLocation,
    sourceCoordinates,
    destinationCoordinates,
    directionData,
    setDirectionData,
  } = useContext(TripContext);

  //Use to Fly to Detination Marker
  useEffect(() => {
    if (destinationCoordinates) {
      mapRef.current?.flyTo({
        center: [destinationCoordinates.lng, destinationCoordinates.lat],
        duration: 2500,
      });
    }

    if (sourceCoordinates && destinationCoordinates) {
      getDirectionRoute();
    }
  }, [destinationCoordinates]);

  //Use to Fly to Source Marker
  useEffect(() => {
    if (sourceCoordinates) {
      mapRef.current?.flyTo({
        center: [sourceCoordinates.lng, sourceCoordinates.lat],
        duration: 2500,
      });
    }
  }, [sourceCoordinates]);

  const getDirectionRoute = async () => {
    const res = await fetch(
      "https://api.mapbox.com/directions/v5/mapbox/driving/" +
        sourceCoordinates.lng +
        "," +
        sourceCoordinates.lat +
        ";" +
        destinationCoordinates.lng +
        "," +
        destinationCoordinates.lat +
        "?overview=full&geometries=geojson" +
        `&access_token=${KEY_MAPBOX}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const result = await res.json();
    setDirectionData(result);
  };
  return (
    <div className="w-full h-full overflow-hidden rounded-md">
      {userLocation ? (
        <Map
          ref={mapRef}
          mapboxAccessToken={`${KEY_MAPBOX}`}
          initialViewState={
            sourceCoordinates
              ? {
                  longitude: sourceCoordinates?.lng,
                  latitude: sourceCoordinates?.lat,
                  zoom: 12,
                }
              : {
                  longitude: userLocation?.lng,
                  latitude: userLocation?.lat,
                  zoom: 12,
                }
          }
          style={{ width: "100%", height: "100%" }}
          mapStyle="mapbox://styles/mapbox/streets-v9"
        >
          <Markers />
          {directionData?.routes ? (
            <MapBoxRoute
              coordinates={directionData?.routes[0]?.geometry?.coordinates}
            />
          ) : null}
        </Map>
      ) : null}
    </div>
  );
}

export default MapboxMap;
