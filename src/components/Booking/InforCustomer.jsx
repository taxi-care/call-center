import React, { useContext, useEffect, useState } from "react";
import Vehicle from "./Vehicle";
import { TripContext } from "../../context/TripContext";
import { BaseUrl, getRequest, postRequest } from "../../utils/services";

function InforCustomer() {
  const [sourceChange, setSourceChange] = useState(false);
  const [destinationChange, setDestinationChange] = useState(false);
  const [addressList, setAddressList] = useState([]);
  const {
    phoneCustomer,
    setPhoneCustomer,
    nameCustomer,
    setNameCustomer,
    setSourceCoordinates,
    setDestinationCoordinates,
    source,
    setSource,
    destination,
    setDestination,
    directionData,
    isReadyBooking,
    tripRequest,
    setHistoryTripByPhone,
  } = useContext(TripContext);

  //Debounce Input phoneNumber
  useEffect(() => {
    const deplayDebounceFn = setTimeout(() => {
      getHistoryCustomer();
    }, 1000);

    return () => clearTimeout(deplayDebounceFn);
  }, [phoneCustomer]);

  const getHistoryCustomer = async () => {
    if (phoneCustomer) {
      const res = await postRequest(
        `${BaseUrl}/trips/get-all-trip`,
        JSON.stringify({ PhoneCustomer: phoneCustomer })
      );
      if (res.error) {
        return console.log(res);
      }
      setHistoryTripByPhone(res?.metadata);
    }
  };

  //Debounce Input source,destination
  useEffect(() => {
    const deplayDebounceFn = setTimeout(() => {
      getAddressList();
    }, 1000);

    return () => clearTimeout(deplayDebounceFn);
  }, [source, destination]);

  const getAddressList = async () => {
    setAddressList([]);
    if (source || destination) {
      const query = sourceChange ? source : destination;
      if (query) {
        const res = await fetch(
          `https://maps.vietmap.vn/api/autocomplete/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&text=${query}`
        );

        const result = await res.json();
        setAddressList(result);
      }
    }
  };

  const onSourceAddressClick = async (item) => {
    setSource(item.display);
    setAddressList([]);
    setSourceChange(false);
    const res = await fetch(
      `https://maps.vietmap.vn/api/place/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&refid=${item.ref_id}`
    );
    const result = await res.json();
    setSourceCoordinates({
      lat: result.lat,
      lng: result.lng,
    });
  };
  const onDestinationAddressClick = async (item) => {
    setDestination(item.display);
    setAddressList([]);
    setDestinationChange(false);
    const res = await fetch(
      `https://maps.vietmap.vn/api/place/v3?apikey=4984c36970bf2ee8648026e6bc029b6ec3766ba2fbcbea4f&refid=${item.ref_id}`
    );

    const result = await res.json();

    setDestinationCoordinates({
      lat: result.lat,
      lng: result.lng,
    });
  };

  return (
    <div className="border-[1px] p-5 rounded-md">
      <div className="w-full">
        <label htmlFor="" className="text-gray-400">
          Phone
        </label>
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-md outline-none focus:border-yellow-300"
          value={phoneCustomer}
          onChange={(e) => {
            setPhoneCustomer(e.target.value);
          }}
        />
      </div>
      <div className="w-full mt-4">
        <label htmlFor="" className="text-gray-400">
          Name
        </label>
        <input
          type="text"
          className="bg-white p-1 border-[1px] w-full rounded-md outline-none focus:border-yellow-300"
          value={nameCustomer}
          onChange={(e) => {
            setNameCustomer(e.target.value);
          }}
        />
      </div>
      <div className="w-full mt-4 relative">
        <label htmlFor="" className="text-gray-400">
          Where to?
        </label>
        <input
          type="text"
          className="bg-white border-[1px] w-full rounded-md outline-none focus:border-blue-300 text-sm p-2"
          value={source}
          onChange={(e) => {
            setSource(e.target.value);
            setSourceChange(true);
          }}
        />
        {addressList.length > 0 && sourceChange ? (
          <div
            className="shadow-md p-1 rounded-md
          absolute w-full bg-white z-20 h-[300px] overflow-auto"
          >
            {addressList?.map((item, index) => (
              <h2
                key={index}
                className="p-3 hover:bg-gray-100 cursor-pointer"
                onClick={() => {
                  onSourceAddressClick(item);
                }}
              >
                {item.display}
              </h2>
            ))}
          </div>
        ) : null}
      </div>
      <div className="w-full mt-4 relative">
        <label htmlFor="" className="text-gray-400">
          Where from?
        </label>
        <input
          type="text"
          className="bg-white p-2 border-[1px] w-full rounded-md outline-none focus:border-blue-300 text-sm"
          value={destination}
          onChange={(e) => {
            setDestination(e.target.value);
            setDestinationChange(true);
          }}
        />
        {addressList.length > 0 && destinationChange ? (
          <div className="shadow-md p-1 rounded-md absolute w-full bg-white z-19 h-[300px] overflow-auto">
            {addressList?.map((item, index) => (
              <h2
                key={index}
                className="p-3 hover:bg-gray-100 cursor-pointer"
                onClick={() => {
                  onDestinationAddressClick(item);
                }}
              >
                {item.display}
              </h2>
            ))}
          </div>
        ) : null}
      </div>
      <div>
        <Vehicle />
      </div>
      <div className="flex justify-between mt-4">
        <h1 className="font-semibold">Distance:</h1>
        <span>
          {directionData?.routes
            ? `${(directionData.routes[0].distance / 1000).toFixed(1)} km`
            : null}
        </span>
      </div>
      <button
        className={`cursor-pointer w-full
       p-2 rounded-md
       mt-4 mx-auto font-semibold ${
         isReadyBooking ? "bg-blue-700 text-white" : "bg-slate-400 text-black"
       }`}
        disabled={isReadyBooking ? false : true}
        onClick={() => tripRequest()}
      >
        Điều phối chuyến đi
      </button>
    </div>
  );
}

export default InforCustomer;
