import React, { useContext } from "react";
import { TripContext } from "../../context/TripContext";
import TripRow from "./TripRow";

function TripTable() {
  const { historyTripByPhone } = useContext(TripContext);
  return (
    <div className="overflow-x-auto w-[98%] mt-5 mx-auto max-h-56 rounded-md">
      <table className="min-w-full bg-white border">
        <thead className="bg-yellow-400 text-white sticky top-[-5px]">
          <tr>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              STT
            </th>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              Created At
            </th>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              Source
            </th>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              Destination
            </th>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              SelectedCar
            </th>
            <th className="text-left py-3 px-4 uppercase font-semibold text-sm">
              Status
            </th>
          </tr>
        </thead>
        <tbody className="text-gray-700 h-20 overflow-y-auto">
          {historyTripByPhone?.map((trip, index) => (
            <TripRow trip={trip} key={index} index={index} />
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default TripTable;
