import React, { useContext, useEffect, useState } from "react";
import { getRequest, BaseUrl } from "../../utils/services";
import { TripContext } from "../../context/TripContext";

function Vehicle() {
  const [vehicleList, setVehicleList] = useState();
  const { directionData, selectedCar, setSelectedCar } =
    useContext(TripContext);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await getRequest(`${BaseUrl}/cars`);
        if (res.error) {
          console.log(res);
        }
        setVehicleList(res.cars);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
  }, []);

  const getCost = (charges) => {
    return (charges * (directionData.routes[0].distance / 1000)).toFixed(1);
  };

  return (
    <div className="grid grid-cols-3 gap-3 mt-4">
      {vehicleList &&
        vehicleList.map((item, index) => {
          return (
            <div
              key={index}
              className={` p-2 border-[2px] rounded-md hover:border-yellow-400 cursor-pointer ${
                item._id == selectedCar?.id
                  ? "border-yellow-400 border-[2px]"
                  : ""
              }`}
              onClick={() =>
                setSelectedCar({ id: item._id, price: item.price })
              }
            >
              <div className="flex justify-center">
                <img
                  src={`/${item.model_name}.svg`}
                  alt={item.model_name}
                  width={50}
                  height={50}
                />
              </div>
              <h2 className="text-[12px] text-gray-500">
                {item.model_name}
                {directionData?.routes ? (
                  <span className="float-right text-black font-medium">
                    {getCost(item.price)}k
                  </span>
                ) : null}
              </h2>
            </div>
          );
        })}
    </div>
  );
}

export default Vehicle;
