import React, { useContext } from "react";
import { useInforCar } from "../../hooks/useInforCar";
import { TripContext } from "../../context/TripContext";

function TripRow({ trip, index }) {
  const { inforCar } = useInforCar(trip);
  const {
    setSource,
    setDestination,
    setSelectedCar,
    setSourceCoordinates,
    setDestinationCoordinates,
    setNameCustomer,
  } = useContext(TripContext);
  const handleOnclickTrip = () => {
    console.log(trip);

    setSource(trip.source);
    setDestination(trip.destination);
    setSelectedCar({ id: trip.selectedCar, price: inforCar.price });
    setSourceCoordinates({
      lng: trip.sourceCoordinates[0],
      lat: trip.sourceCoordinates[1],
    });
    setDestinationCoordinates({
      lng: trip.destinationCoordinates[0],
      lat: trip.destinationCoordinates[1],
    });
    setNameCustomer(trip.NameCustomer);
  };
  return (
    <tr
      className="cursor-pointer hover:bg-slate-100"
      onClick={() => handleOnclickTrip()}
    >
      <td className="text-left py-3 px-4">{index + 1}</td>
      <td className="text-left py-3 px-4">
        {new Date(trip.createdAt).toLocaleString()}
      </td>
      <td className="text-left py-3 px-4">{trip.source}</td>
      <td className="text-left py-3 px-4">{trip.destination}</td>
      <td className="text-left py-3 px-4">{inforCar?.model_name}</td>
      <td className="text-left py-3 px-4">{trip.status}</td>
    </tr>
  );
}

export default TripRow;
