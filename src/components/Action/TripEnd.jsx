import React from "react";
import moment from "moment";
import { useInforCar } from "../../hooks/useInforCar";

function TripEnd({ trip }) {
  const { inforCar } = useInforCar(trip);
  return (
    <div className="bg-slate-100 rounded-md flex flex-col justify-center shadow-md w-[95%] p-3">
      <div className="flex items-center border-b border-gray-200 gap-3">
        {inforCar ? (
          <img
            src={`/${inforCar?.model_name}.svg`}
            alt=""
            className="w-8 h-8"
          />
        ) : null}
        <div className="flex grow justify-between items-center">
          <h1>
            <span>{moment(trip?.createdAt).calendar()}</span> |{" "}
            <span className="font-semibold">{trip?.price}k</span>
            <p>{inforCar?.model_name}</p>
          </h1>
          <div className="flex gap-3">
            <span className="text-red-400">
              {moment().diff(moment(trip?.createdAt), "minutes") > 5 &&
              trip.status == "pending"
                ? "More than 5 minutes have passed without finding a driver for the trip"
                : null}
            </span>
            <span
              className={`p-1 rounded-md ${
                trip.status == "cancel"
                  ? "bg-red-200 text-red-500"
                  : trip.status == "pending"
                  ? "bg-yellow-200 text-yellow-500"
                  : "bg-green-200 text-green-500"
              }`}
            >
              {trip.status}
            </span>
          </div>
        </div>
      </div>
      <p className="text-ellipsis whitespace-nowrap overflow-hidden mt-4">
        {trip?.source}
      </p>
      <p className="text-ellipsis whitespace-nowrap overflow-hidden mt-2">
        {trip?.destination}
      </p>
    </div>
  );
}

export default TripEnd;
