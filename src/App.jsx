import { useContext, useState } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { AuthContext } from "./context/AuthContext";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Booking from "./pages/Booking";
import Action from "./pages/Action";
import { TripContextProvider } from "./context/TripContext";

function App() {
  const { user } = useContext(AuthContext);
  return (
    <TripContextProvider user={user}>
      <Routes>
        <Route path="/" element={user ? <Home /> : <Login />}>
          <Route path="/" element={<Booking />} />
          <Route path="/action" element={<Action />} />
        </Route>
        <Route path="/login" element={user ? <Home /> : <Login />} />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
    </TripContextProvider>
  );
}

export default App;
